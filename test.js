$(function () {
    var slickOpts = {
        dots: true,
        infinite: true,
        speed: 500,
        autoplay: true
    };
    // Init the slick
    $('.single-item').slick(slickOpts);
    var slickEnabled = true;
   
});